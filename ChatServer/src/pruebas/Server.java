package pruebas;

import java.io.*;
import java.net.*;
public class Server 
{
	public static void main(String args[])
	{
		ServerSocket server;
		Socket connection;
		DataOutputStream output;
		DataInputStream input;
		int counter = 1;
		try {
			// Creamos un ServerSocket
			server = new ServerSocket(5000, 100);
			System.out.println("Escuchando en 5000:100");
			while (true) 
			{
				// Esperamos a la conexi�n
				connection = server.accept();
				System.out.println("Connexi�n " + counter + " recibida de:"
						+ connection.getInetAddress().getHostName());
				// Abrimos los canales de E/S
				input = new DataInputStream(connection.getInputStream());
				output = new DataOutputStream(connection.getOutputStream());
				// Utilizamos los m�todos de Write y de Read
				output.writeUTF("Conexi�n exitosa");
				System.out.println("Mensaje del cliente " + input.readUTF());
				// Cerramos la conexi�n
				connection.close();
				++counter;
			}
		} catch (IOException e) {}
	}
}