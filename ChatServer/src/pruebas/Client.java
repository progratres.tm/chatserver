package pruebas;

import java.io.*;
import java.net.*;
public class Client 
{
	public static void main(String args[]) 
	{
		Socket client;
		DataOutputStream output;
		DataInputStream input;
		try {
			// Creamos el socket para establecer la conexi�n
			client = new Socket(InetAddress.getLocalHost(), 5000);
			// Abrimos los canales de E/S
			input = new DataInputStream(client.getInputStream());
			output = new DataOutputStream(client.getOutputStream());
			// Utilizamos los m�todos de las clases Read y Write
			System.out.println("Mensaje del Servidor " + input.readUTF());
			output.writeUTF("Muchas Gracias :) ");
			// Cerramos la conexi�n
			client.close();
		} catch (IOException e) {
		}
	}
}